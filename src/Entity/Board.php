<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use App\Entity\Utils\CreatedByInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "get",
 *         "post"={"security"="is_granted('ROLE_USER')"},
 *     },
 *     itemOperations={
 *         "get",
 *         "put"={
 *          "access_control"="is_granted('IS_AUTHENTICATED_FULLY') and object.created_by == user",
 *          "denormalization_context"={
 *              "groups"={"put"}
 *          },
 *          "normalization_context"={
 *              "groups"={"get"}
 *           }
 *         },
 *
 *     },
 *     denormalizationContext={
 *         "groups"={"post"}
 *     },
 *     normalizationContext={
 *         "groups"={"get"}
 *     },
 * )
 * @ORM\Entity(repositoryClass="App\Repository\BoardRepository")
 * @UniqueEntity("name", groups={"post"})
 * @UniqueEntity("slug", groups={"post"})
 */
class Board implements CreatedByInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=31, unique=true)
     * @Groups({"get", "post"})
     * @Assert\NotBlank(groups={"post"})
     * @Assert\Length(
     *     min="3",
     *     max="20"
     * )
     */
    private $name;

    /**
     * @Groups({"get"})
     * @ORM\Column(type="string", length=31, unique=true)
     */
    private $slug;

    /**
     * @Groups({"get", "post"})
     * @Assert\NotBlank(groups={"post"})
     * @Assert\Length(
     *     min="3",
     *     max="20"
     * )
     * @ORM\Column(type="string", length=63, nullable=true)
     */
    private $title;

    /**
     * @Groups({"get", "post", "put"})
     * @Assert\Length(
     *     min="3"
     * )
     * @ORM\Column(type="text", length=1023, nullable=true)
     */
    private $description;

    /**
     * @ApiSubresource(maxDepth=1)
     * @ORM\OneToMany(targetEntity="App\Entity\Post", mappedBy="board", orphanRemoval=true)
     */
    private $posts;

    /**
     * @var UserInterface
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="boards")
     * @ORM\JoinColumn(nullable=false)
     */
    private $createdBy;

    /**
     * @Groups({"get", "post"})
     *
     * @ORM\ManyToOne(targetEntity=MediaObject::class)
     * @ORM\JoinColumn(nullable=true)
     * @ApiProperty(iri="http://schema.org/image")
     */
    public $icon;

    public function __construct()
    {
        $this->posts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return Collection|Post[]
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }

    public function addPost(Post $post): self
    {
        if (!$this->posts->contains($post)) {
            $this->posts[] = $post;
            $post->setBoard($this);
        }

        return $this;
    }

    public function removePost(Post $post): self
    {
        if ($this->posts->contains($post)) {
            $this->posts->removeElement($post);
            // set the owning side to null (unless already changed)
            if ($post->getBoard() === $this) {
                $post->setBoard(null);
            }
        }

        return $this;
    }

    /**
     * @return MediaObject|null
     */
    public function getIcon(): ?MediaObject
    {
        return $this->icon;
    }

    /**
     * @param MediaObject|null $icon
     */
    public function setIcon(?MediaObject $icon): void
    {
        $this->icon = $icon;
    }

    public function setCreatedBy(UserInterface $user): CreatedByInterface
    {
        $this->createdBy = $user;
        return $this;
    }

    /**
     * @return UserInterface
     */
    public function getCreatedBy(): UserInterface
    {
        return $this->createdBy;
    }

    /**
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Board
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;
        return $this;
    }
}
