<?php

namespace App\Tests\api;

use App\Entity\User;
use App\Tests\ApiTester;
use Codeception\Example;
use Codeception\Util\HttpCode;
use Symfony\Component\Security\Core\Encoder\NativePasswordEncoder;
use Symfony\Component\Security\Core\Encoder\PlaintextPasswordEncoder;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserCest
{
    public function create(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->haveHttpHeader('Accept', 'application/json');
        $I->sendPOST('users', [
            'username' => 'UserName',
            'email' => 'user@email.domain',
            'password' => 'Secret!23',
        ]);
        $I->seeResponseCodeIs(HttpCode::CREATED);
        $I->seeResponseIsJson();

        $user = $I->grabFromRepository(User::class, 'password', ['username' => 'UserName']);
    }


    /**
     * @dataProvider provideUsers
     */
    public function loginUser(ApiTester $I, Example $example)
    {
        //Used to create user mock with encoded password
        $nativePasswordEncoder = new NativePasswordEncoder();
        $password = $nativePasswordEncoder->encodePassword('Secret!23', null);

        $I->haveInRepository(User::class, [
            'username' => $example['username'],
            'email' => $example['email'],
            'password' => $password,
            'enabled' => $example['enabled'],
        ]);

        $I->haveHttpHeader('Accept', 'application/json');
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPOST('login', [
            'username' => $example['username'],
            'password' => 'Secret!23',
        ]);

        if($example['enabled']) {
            $I->wantToTest('User Enabled');
            $I->seeResponseCodeIs(HttpCode::OK);
            $I->seeResponseIsJson();
            $I->seeResponseMatchesJsonType([
                'token' => 'string',
                'refreshToken' => 'string',
            ]);
        } else {
            $I->wantToTest('User Disabled');
            $I->seeResponseCodeIs(HttpCode::UNAUTHORIZED);
        }

    }

    private function provideUsers(): array
    {
        return [
            ['username' => 'User1', 'email' => 'user1@email.domain', 'enabled' => false],
            ['username' => 'User2', 'email' => 'user2@email.domain', 'enabled' => true],
        ];
    }
}
