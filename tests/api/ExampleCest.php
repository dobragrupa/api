<?php

namespace App\Tests\api;

use App\Entity\Example;
use App\Tests\ApiTester;
use Codeception\Util\HttpCode;

class ExampleCest
{
    public function getList(ApiTester $I)
    {
        $I->haveHttpHeader('Accept', 'application/json');
        $I->sendGET('examples');
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
    }

    public function post(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->haveHttpHeader('Accept', 'application/json');
        $I->sendPOST('examples', [
            'name' => 'name',
            'description' => 'description',
            'birthAt' => '2020-03-19T20:28:33.122Z'
        ]);
        $I->seeResponseCodeIs(HttpCode::CREATED);
        $I->seeResponseIsJson();
    }

    public function put(ApiTester $I)
    {
        $I->haveInRepository(Example::class, [
            'id' => 1,
            'name' => 'name',
            'description' => 'description',
            'birthAt' => new \DateTime()
        ]);

        $I->haveHttpHeader('Accept', 'application/json');
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPUT('examples/1', [
            'name' => 'updated',
            'description' => 'description',
            'birthAt' => '2020-03-19T20:28:33.122Z'
        ]);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
    }

    public function get(ApiTester $I)
    {
        $I->haveInRepository(Example::class, [
            'id' => 1,
            'name' => 'name',
            'description' => 'description',
            'birthAt' => new \DateTime()
        ]);

        $I->haveHttpHeader('Accept', 'application/json');
        $I->sendGET('examples/1');
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
    }

    public function delete(ApiTester $I)
    {
        $I->haveInRepository(Example::class, [
            'id' => 1,
            'name' => 'name',
            'description' => 'description',
            'birthAt' => new \DateTime()
        ]);

        $I->haveHttpHeader('Accept', 'application/json');
        $I->sendDELETE('examples/1');
        $I->seeResponseCodeIs(HttpCode::NO_CONTENT);

        $I->haveHttpHeader('Accept', 'application/json');
        $I->sendGET('examples/1');
        $I->seeResponseCodeIs(HttpCode::NOT_FOUND);
        $I->seeResponseIsJson();
    }
}
