<?php

namespace App\Tests\Unit\EventSubscriber;

use App\Entity\User;
use App\Security\UserEnabledChecker;
use Codeception\Stub;
use Codeception\Test\Unit;
use Symfony\Component\Security\Core\Exception\DisabledException;

class UserEnabledCheckerTest extends Unit
{
    public function testUserIsDisabled()
    {
        /** @var User $user */
        $user = Stub::make(User::class, [
            'enabled' => false,
        ]);

        $this->expectException(DisabledException::class);
        $userEnabledChecker = new UserEnabledChecker();
        $userEnabledChecker->checkPreAuth($user);
    }
}
